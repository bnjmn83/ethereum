module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*", // Match any network id
      from: "0x10e13D31e6D11514777C666381603f0791D36761",
      gasPrice: 1,
      gas: 6000000    }
  }
};