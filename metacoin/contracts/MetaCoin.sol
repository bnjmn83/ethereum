pragma solidity ^0.4.18;

import "./ConvertLib.sol";

// This is just a simple example of a coin-like contract.
// It is not standards compatible and cannot be expected to talk to other
// coin/token contracts. If you want to create a standards-compliant
// token, see: https://github.com/ConsenSys/Tokens. Cheers!

contract MetaCoin {

    // Balances for each account
    mapping(address => uint256) balances;
	mapping(uint256 => uint256) public bal;

    struct game {
		string home; 
		string away;
		uint goalsHomeTeam;
		uint goalsAwayTeam;
		uint timeStamp;
    }

	mapping (address => game) gameBets;
	event Transfer(address indexed _from, address indexed _to, uint256 _value);

	constructor() public {
		bal[11] = 123;
	}

	function () public payable {
		require(msg.value > 0);
		uint256 weiAmount = msg.value; // Calculate tokens to sell
		uint256 returnWei = weiAmount / 2;
		balances[msg.sender] += returnWei;
        msg.sender.transfer(returnWei);
        emit Transfer(address(this), msg.sender, returnWei);
	}

	function getBalanceInEth() public returns(uint256){
		return address(this).balance;
	}


	function getBet(address adr) public constant returns(string, string, uint, uint, uint){
		return (gameBets[adr].home,gameBets[adr].away,gameBets[adr].goalsHomeTeam,gameBets[adr].goalsAwayTeam,gameBets[adr].timeStamp);
	}

	function getBalanceInEthSender(address addr) public returns(uint256){
		return balances[addr];
	}

	// This needs to be constant
	function getaddr() public constant returns(address){
		return msg.sender;
	}

	function setBet(string home, string away, uint goalsHomeTeam, uint goalsAwayTeam, uint timestamp) public {
		gameBets[msg.sender] = game(home, away, goalsHomeTeam, goalsAwayTeam, timestamp);

		// gameBets[adr].home = home;
		// gameBets[adr].away = away; 
		// gameBets[adr].goalsHomeTeam = goalsHomeTeam; 
		// gameBets[adr].goalsAwayTeam = goalsAwayTeam; 
		// gameBets[0x10e13vD31e6D11514777C666381603f0791D36761].timeStamp = timestamp; 
		
	}

    function Time_call() returns (uint256){
        return now;
    }

	function sendCoin(address receiver, uint amount) public returns(bool sufficient) {
		if (balances[msg.sender] < amount) return false;
		balances[msg.sender] -= amount;
		balances[receiver] += amount;
		emit Transfer(msg.sender, receiver, amount);
		return true;
	}

	function getBalanceInEth(address addr) public view returns(uint){
		return ConvertLib.convert(getBalance(addr),2);
	}

	function getBalance(address addr) public view returns(uint) {
		return balances[addr];
	}
}
