const artifacts = require('../build/contracts/MetaCoin.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

module.exports = function(callback) {
    var meta;
    var creator = web3.eth.accounts[0]
    var purchaser = web3.eth.accounts[1]

    MyContract.deployed().then(function(instance) {
        meta = instance;
        return meta.getBalance.call(creator);
    }).then(function(result) {
        console.log("acc one: " + result)
        return meta.getBalance.call(purchaser);
    }).then(function(result) {
        console.log("acc two: " + result)
    }).catch(function(e) {
        console.log(e)
    })

    // Not sure when to use; does not replace "{from: creator}" in sendCoin fct
    // web3.eth.defaultAccount = creator

    MyContract.deployed().then(function(instance) {
        meta = instance;
        // return meta.sendCoin(purchaser, 1);
        console.log("Send 1 coin");
        return meta.sendCoin(purchaser, 1, {from: creator});
    }).then(function(result) {
        return meta.getBalance.call(creator);
    }).then(function(result) {
        console.log("acc one: " + result)
        return meta.getBalance.call(purchaser);
    }).then(function(result) {
        // Blocks get mined slower so we wont see the change in the purchaser account here
        console.log("acc two: " + result)
        return callback();
    }).catch(function(e) {
        console.log(e)
    })
}