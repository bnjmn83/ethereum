const artifacts = require('../build/contracts/MetaCoin.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

module.exports = function(callback) {
    var meta;
    var contract;
    var creator = web3.eth.accounts[0]
    var purchaser = web3.eth.accounts[0]

    var seconds = new Date().getTime() / 1000;
    web3.eth.defaultAccount = web3.eth.accounts[0]

    // MyContract.deployed().then(function(instance) {
    //     meta = instance;
    //     contract = instance.address;
    //     console.log("contract address: " + instance.address)

        MyContract.deployed().then(function(instance) {
            meta = instance;
            return meta.getBalanceInEth.call();
        }).then(function(result) {
            console.log("Overall balance: " + web3.fromWei(result, "ether"))
            return meta.getBalanceInEthSender.call(purchaser);
        }).then(function(result) {
            console.log("Sender balance: " + web3.fromWei(result, "ether"))
            return meta.setBet.sendTransaction("sge ", "bmg ", 2, 1, seconds,{from: purchaser, gas:6000000,  gasPrice: '8'})
        }).then(function(result) {
            console.log(result)
            console.log("set bet: " + "Frankfurt " + "M'gladbach " + "2 " + "1 " + seconds.toString())
            return meta.getaddr.call({from: purchaser})
        }).then(function(result) {
            console.log("get addr " + result)
            console.log(purchaser)
            return meta.getBet.call(purchaser, {from: purchaser, gas:3000000})
        }).then(function(result) {
            console.log("get bet: " + result)
        }).catch(function(e) {
            console.log(e)
        })
}

    //     web3.eth.sendTransaction({
    //         from: purchaser,
    //         to: contract,
    //         value: web3.toWei(2, "ether"),
    //     }, function(err, transactionHash) {
    //         if (err) { 
    //             console.log(err); 
    //         } else {
    //             console.log(transactionHash);
    
    //             MyContract.deployed().then(function(instance) {
    //                 meta = instance;
    //             //     return meta.getBalanceInEth.call();
    //             // }).then(function(result) {
    //             //     console.log("Overall balance: " + web3.fromWei(result, "ether"))
    //             //     return meta.getBalanceInEthSender.call(purchaser);
    //             // }).then(function(result) {
    //                 // console.log("Sender balance: " + web3.fromWei(result, "ether"))
    //                 // Set from or get invalid address and set gas or get out of gas error
    //                 return meta.setBet.call("sge","bmg", 2, 1, seconds, {from: purchaser, gas:6000000})
    //             }).then(function(result) {
    //                 console.log(result)
    //             //     console.log("set bet: " + "sge " + "bmg " + "2 " + "1 " + seconds.toString())
    //             //     return meta.getaddr.call({from: purchaser})
    //             // }).then(function(result) {
    //             //     console.log("get addr " + result)
    //             //     console.log(purchaser)
    //                 return meta.getBet.call({from: purchaser, gas:3000000})
    //             }).then(function(result) {
    //                 console.log("get bet: " + result)
    //             }).catch(function(e) {
    //                 console.log(e)
    //             })            
    //         }
    //     });        
    // }).catch(function(e) {
    //     console.log(e)
    // }) 

// }