// var Web3 = require('web3');
// var web3 = new Web3();
// var contracts = require(path.join(__dirname, 'gen_contracts.json'));
// // First we instruct web3 to use the RPC provider
// web3.setProvider(
//   new web3.providers.HttpProvider(
//     'http://' + opts.rpc_host + ':' + opts.rpc_port));


const artifacts = require('../build/contracts/MetaCoin.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

// This listens for the latest transaction on the blockchain
module.exports = function(callback) {
  var filter = web3.eth.filter("latest",function(error, blockHash) {
    if (!error) {
        var block = web3.eth.getBlock(blockHash, true);        
        if (block.transactions.length > 0) {
            console.log("found " + block.transactions.length + " transactions in block " + blockHash);
            console.log(JSON.stringify(block.transactions));
        } else {
            console.log("no transaction in block: " + blockHash);
        }
    }
});
}


/*
// Here we register the filter with the ethereum node, and then begin polling for updates.
function runLoop(o) {
  var filter = web3.eth.filter({address: o.contract_address});
  filter.watch(function (err, results) {
    if (err) {
      console.log('WATCH ERROR: ', err);
      process.exit();
    }
    console.debug(results);
  });
}
// If the contract isn't deployed yet, we deploy it here
if (!opts.contract_address) {
// This block of code loads the ABI for interpreting contract data.
  var dmC = web3.eth.contract(JSON.parse(contracts.DomainMicropay.abi));
  var x = {
    from: web3.eth.coinbase,
    data: contracts.DomainMicropay.bin,
    gas: 1000000
  };
  // send the transaction for installing the contract.
  dmC.new(x, function (err, resp) {
    if (err) {
      console.error('Loading contract', err);
      process.exit();
    }
    var addr = resp.address;
    if (!addr) {
      console.log('Pending tx: ', resp.transactionHash);
    } else {
      console.log('Deployed Address: ', addr);
      opts.contract_address = addr;
      runLoop(opts);
    }
  });
} else {
  runLoop(opts); // in either case, start the polling event loop.
}

*/