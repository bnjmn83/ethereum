# Peer 2 Peer betting using Smart Contracts and Oraclize
This is a first try to combine SC and an Oracle to show the capabilities of this young technology. As a simple "getting-in-touch" topic a betting use case has been choosen. 

# Abstract
This project tries to provide a way for two persons to bet against each other on football matches in a decentralized way. Therefore, both parties have to interact with a SC that must be deployed to the Blockchain beforehand. 
The flow is as follows:

* Person A 
    * will select a match and and retrieve the regarding match id from  _http://api.football-data.org_
    * will send a transaction to the SC with the match ID, the expected match score and a certain amount of Ether to place the bet
    * will receive a confirmation ID which can be passed to Person B to participate in the bet
* Person B
    * will use the bet confirmation ID from Person A to send a transaction to the SC using this ID, the expected match score and a certain amount of Ether
* The SC will verify if the bets are valid

# Technologies
To demonstrate the main features of the Ethereum Blockchain this project combines multiple technologies and frameworks. There is truffle which supports the developement of Smart Contracts in Solidity and its migration to the Blockchain. There is Oraclize that acts as an Oracle to get real life events into the Blockchain. As a private Blockchain we use Ganache. Several javascript packages help us to interact with the Blockchain for testing purposes. Last but not least we use the API from _http://api.football-data.org_ as an endpoint for our Oracle to gather real life football event data.

# Setting up the environment
Checkout this repo via:

    git clone https://gitlab.com/bnjmn83/ethereum.git

The submodules can be ignored and don't need to be updated.

## NPM and Node
Install latest version.

## Ganache 
_Download and install the client: https://truffleframework.com/ganache_

It is a personal blockchain for Ethereum development that runs on your desktop. It makes developement easy and fast. Mining blocks is done instantly. There is also a nice UI available to verify transactions.

Using Ganache you can quickly see how your application affects the blockchain, and introspect details like your accounts, balances, contract creations and gas costs

## Truffle Suite
The official docs:

    https://truffleframework.com/docs/truffle/getting-started/installation


The github monorepo:
    
    https://github.com/trufflesuite/truffle

Install _truffle_ suite


    npm install -g truffle


Install _truffle-contract_ which enhances contract abstraction.

    npm install -g truffle-contract

### Windows
It is mandatory for the _ethereum-bridge_ module to install this package in an admin shell:

    npm install --global --production windows-build-tools

### MAC
TBD

### Ubuntu 
TBD

## Ethereum-Bridge
Install NPM package

    npm install -g ethereum-bridge

Run in dedicated shell window:

    ethereum-bridge -H localhost:7545 -a 9 --instance latest

_For more information on the arguments see  [here](#ethereumbridgedetails)_

Make sure the window prints the following lines:

    Please add this line to your contract constructor:
    OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
  
Open the file:

    ./contracts/DieselPrice.sol

and adjust the constructor with the above hex value:

        constructor () public {
          OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
        }

The bridge is now running and ready to accept Oraclize requests.


# Running the project
## Deploy contract
Start local Blockchain:
    
    ./Ganache

cd into project folder:

    ~/ethereum/oracalize-diesel 

Build all contracts in _contracts_ folder:   

    truffle compile --all

Deploy contracts to Blockchain. Truffle will walk through the _migrations_ folder and execute all of them:

    truffle migrate --reset

The output should be like this:

    Running migration: 1_initial_migration.js
      Replacing Migrations...
      ... 0x2344fc0779247d9d6ef17330bd758700393878cbd853615cf708c86763a0c2d5
      Migrations: 0x5012fd707ca12ad001e8155de5ddfa65f30157f7
    Saving successful migration to network...
      ... 
    Saving successful migration to network...
      ... 0xb2734d82d4f954d338e6cc4ff4e6de35f51cb70fbff7f61545bec74402fad945
    Saving artifacts...

Open the Ganache client and check the transaction window. Each contract deployment is executed as a transaction. Also check the 10. account in Ganache and check its balance. When we ran the ethereum-bridge-client we specified this account as initiator so the balance for each transaction was taken from this account.

## Sending transactions to the Contract
Open a shell and execute:

     truffle exec .\scripts\watch.js

This will start a watcher that listens to events from the Blockchain.
Open another shell and run:

     truffle exec .\scripts\place_bet.js

This will place a bet on the contract.
Check Ganache transaction window to verify it.
Check the watcher shell for the following events:
_Note: The second event might take a few seconds/minutes to show up since it interacts with the web_

    { logIndex: 0,
      transactionIndex: 0,
      transactionHash: '0xb5bac0e0fe7307688a7447b322c423bc46282c71cf82b261a45ecb22f246fb19',
      blockHash: '0xdd765fd529beb1185e6f9c6545cec5ce33777ff2f5aba9bc8ada1af645e3f558',
      blockNumber: 22,
      address: '0x634f8e14c812ca578bae5b34bfd0c01340a55973',
      type: 'mined',
      event: 'callbackInvocation',
      args:
      { id: '0x8aec51c264d629211b86f7c3815d9f8bf54d52d3127e3b3f2edd250650eaad50',
        result: 'SCHEDULED' } }
    
    { logIndex: 1,
    ...
      args: { msg: 'Bet successfull' } }

This output indicates a successful bet. The id from the first event needs to be given to anyone who wants to participate in this bet:

    id: '0x8aec51c264d629211b86f7c3815d9f8bf54d52d3127e3b3f2edd250650eaad50'

Now join the bet by using the above. Open the file:

    ./scripts/join_bet.js

Change:

    var betId = '0x8aec51c264d629211b86f7c3815d9f8bf54d52d3127e3b3f2edd250650eaad50';

and then invoke:

    truffle exec ./scripts/join_bet.js

The watcher windows will show:

    { logIndex: 2,
      ...
      event: 'joinBetEvent',
      args: { msg: 'Bet to join found' } }
    { logIndex: 0,
      ...
      args:
      { id: '0x02cf6a171eabe4c6569c5f83f54979a38ab1ea97e9f9b1664efee24facee25da',
        result: 'SCHEDULED' } }
    { logIndex: 1,
      ...,
      args: { msg: 'Bet successfull' } }

Congratulation you initiated your first bet on the match VFL Wolfsburg vs. Eintracht Frankfurt taking place at 2019-04-19T00:00:00Z.


# Implementation Details
## Oraclize
For a bet to be placed an anchor of trust is needed that not only provides information about upcoming events in a reliable way but also needs to be trusted. For this requirement _http://api.football-data.org_ was choosen. Since this is a single source of trust it is not advisable to deploy such a scenario into production but rather build a pool of trust anchors. Then the decision if a event in real life has taken place can be decided via a majority decision, e.g. 3 out of 5 source agree on a certain event.

So when the user places a bet we need to verify it first via a _computation_ query which can be used fo arbitrary computation. Oraclize created an application which is stored using IPFS. This application is identified within IPFS using a hash. Oraclize will pull the zipped application from the storage and run it on a sandboxed Amazon Web Service virtual machine. 

The Oraclize query looks as follows:

        queryId = oraclize_query("computation",
            "json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).match.status",
            "GET",
            "http://api.football-data.org/v2/matches/235731",
            "{'headers': {'X-Auth-Token': '2dd267c2daed49eb9f1ba4b40ef89347'}}
        ");

For more information of the computation data source see _https://docs.oraclize.it/#data-sources-computation_. 

The query will take a while before Oraclize will report back the status of the match via a callback. The result of the query based on the given IPFS hash is defined via the second argument of the query:

        json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).match.status

This tells Oraclize to deliver the result as JSON and filter for the match status within the JSON object.

The callback will also provide the _queryId_ which was initially returned by the query when executed. This can be used to identify single callbacks.

The result of the query is a string which can be either "SCHEDULED" or "FINISHED". The ladder indicates that the match has already been taken place and hence, the SC will not accept the bet.

_For more information about Oraclize see [here](#oraclizedetails)_

## Initialize Oracle in code
if you are trying to do a sendTransaction to the OraclizeI oraclize state variable before it is actually set, it will not work so either do a first interaction with Oraclize in the ctor or init the state variable. One way to do so is by setting the proof type or just calling oraclize_setNetwork() to set the OAR, and then setting the oraclize variable using it:

        oraclize_setNetwork(networkID_auto);

## API
### setBet(string matchId, string goalsHomeTeam, string goalsAwayTeam)
TBD

### joinBet(string betId, string matchId, string goalsHomeTeam, string goalsAwayTeam)
TBD

# Advanced Usage
## Football-Data.org
Before the bet can be placed a football match and its match ID needs to acquired. E.g. Execute the following command to get all matches for a specific match day:

        curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/competitions/BL1/matches?matchday=6 

Select a match and get its _id_. Verify the ID and make sure the match is not labeled as _FINISHED_ but as _SCHEDULED_:

        curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/matches/<id>

Now the bet can be placed using javascript and the _truffle-contract_ package. 

## Placing a bet
When the _setBet_ function is used to place a bet the user needs to provide the match ID (see chapter Football-Data.org) and the prediceted result. To place an actual bet a transaction has to be made to the address of the SC. Since this transaction will change the SC's state we need to send our bet along with some Ether to pay for the state change. Additionally, we specify the GAS price we are willing to pay and the amount of GAS

# Artifacts
This chapter will provide a more detailed view on the most important artifacts of this project

<a id="ethereumbridgedetails"></a>

## Ethereum-Bridge
To interface _football-data.org_ Oraclized is used. Since this project not yet runs on a public Blockchain an additional service needs to be available when interaction with Oraclize. AS a provider web3 is used. See _Ethereum-Bridge_ on how to run the service. When starting the 

If you are using a private blockchain, you need something to bridge it with the Oraclize service. Oraclize provides a utility for this, in the case that you're using an Ethereum blockchain, you can use the ethereum-bridge.

Here is a comprehensive guide on how to setup the bridge with testrpc. In the case you're using testrpc, you can use that guide, if it's a private blockchain using a different client, then just skip the testrpc steps, although you will still want an unlocked account accessible, to be able to deploy the required contracts on your privatenet. Link to the guide: https://ethereum.stackexchange.com/a/11389/5819

Marco from Oraclize here. By default the most recent version of the Ethereum Bridge automatically deploys in your private blockchain the Oraclize Address Resolver and the Oraclize Connectors, which are two contracts used by Oraclize to interact with your contract. You don't need to include the OAR in your contract constructor.

If the configuration is correct, the Ethereum-Bridge should log "INFO deploying the address resolver with a deterministic address..." in the command line. The address specified when you launch the bridge is only used to deploy the OAR and the connector, it is not the address of the OAR. You just shouldn't use it to deploy your contract. I have responsed to your question?

### Command line arguments
ethereum-bridge -H localhost:7545 -a 9 --instance latest

<a id="oraclizedetails"></a>

## Oraclize 
Private blockchain guide:

https://ethereum.stackexchange.com/questions/11383/oracle-oraclize-it-with-truffle-and-testrpc/11389#11389

Get latest API version from Oraclize

https://github.com/oraclize/ethereum-api/blob/master/oraclizeAPI_0.5.sol

Rename it to usingOraclie.sol and place it in your contract folder

To test queries one can use the online testing API:

![asd](./res/oraclize-testing-api.PNG "http://app.oraclize.it/home/test_query#")

_http://app.oraclize.it/home/test_query#_

Check the state of a query or review finished queries:

http://app.oraclize.it/home/check_query?id=70df4640d1865f7a88730c53946e7dde37ca2fa07544fe2fb73a62c692ad614c

A query that responds with JSON payload can be parsed using the JSONPATH specs:

    json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).match.score[fullTime][awayTeam]

or

    json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).match.score[fullTime].homeTeam

The _computation_ query can take up to 2 minutes. Any query that needs to send a custom http header along the the GET request needs to use the _computation_ data source instead of the _URL_ data source.

An _URL_ query can look like this. Note, it's payable because we need funds to send to the contract to process the query:

    function update() public payable {
        oraclize_query(60, "URL", "json(https://api.random.org/json-rpc/1/invoke)
        .result.random.data.0", '\n{"jsonrpc":"2.0","method":"generateIntegers","params":
        {"apiKey": "b6f95201-7f37-4ecc-a897-33db429fa65f","n":1,"min":0,"max":10,
        "replacement":true,"base":10},"id":1}');
    }

## Truffle
Check Solidity version

    truffle version
    
Run user scripts. The exec command injects a web3 provider. You'd just have to create the contract instance to use it:

    const artifacts = require('../build/contracts/MetaCoin.json')
    const contract = require('truffle-contract')
    const MyContract = contract(artifacts);
    MyContract.setProvider(web3.currentProvider);

Then the contract can be used:

    MetaCoin.deployed().then(function(instance) {console.log(instance);});

Execute the .js file:

    truffle exec run.js

### Truffle developer cli
This tells you you're running within a Truffle console using the development network.

    > truffle console

All commands that return a promise will automatically be resolved, and the result printed, removing the need to use .then() for simple commands. For example, the following command:

    > DieselPrice.at("0x2f6c6b75937b9a9b8e5d34f04c14cb78c8e3244b").gameBets.call("123")

To find the address of the contract see the console output of the deployment shell:

    Running migration: 1_initial_migration.js
    ...
    Replacing DieselPrice...
    ... 0xb331f0b62170bd50edd23a817d601e446f38c552fe1b50e97c29ef8ee19dfcf5
    _DieselPrice: 0x2f6c6b75937b9a9b8e5d34f04c14cb78c8e3244b_
    ...
    Saving artifacts...

In this case the address is:

    DieselPrice: 0x2f6c6b75937b9a9b8e5d34f04c14cb78c8e3244b

### WEB3
Under the hood a web3 object communicates to a local node through RPC calls.

    > truffle console
    > web3.eth.getBlock('latest').gasLimit

Send ether to contract

    web3.eth.sendTransaction({from: web3.eth.accounts[3], to: "", value: web3.toWei(1, "ether")});

Read from contract at its address

    benstoken.at("").balanceOf(web3.eth.accounts[3])
    benstoken.at("").totalSupply()
    benstoken.at("").balanceOf(web3.eth.accounts[0]).then(function(res){return res.toString();})

Using promise objects; those are return by then(). function(res) gets invoked if promised gets fullfilled.
    
    benstoken.at("").then(function(res) { return res.totalSupply(); }).then(function(result){console.log("got balance", result.toString(10));});

Change sender of transaction in truffle
    
    benstoken.at("0x847b2745c73355fd041a3ca269981e51167a64b2").acceptOwnership({from: web3.eth.accounts[1]})

Transfer Ownsership from:
1. Approve sender 0x71 to send tokens of 1 Ether in total

    benstoken.at("").then(function(res) { return res.approve("", web3.toWei(1, "ether"));}).then(function(result){console.log("cool");});

2. Transfer from 0x71 to 0x26 of max 1 ether; this is what we approved before

    benstoken.at("").then(function(res) { return res.transferFrom("", "", web3.toWei(1, "ether"));}).then(function(result){console.log("cool");});

Call a mapping with given key:

    DieselPrice.at("0x2f6c6b75937b9a9b8e5d34f04c14cb78c8e3244b").gameBets.call("123")

# truffle migrate --compile-all --reset
# truffle build --all
# truffle compile --all
# truffle migrate --reset
# truffle debug TXHash
# truffle console

## Solidity
### CTOR
The constructor is executed during contract deployment. If the contract is deployed successfully, then the constructor has run.

# Things to be discussed
## Validation
Who verifies the validity of a bet? If a person places a bet or bets on an already existing bet we need to check things like:
* is this game valid
* has the game already started

This can be done in two ways. The first approach is the one currently implemented and uses Oraclize to query an API. Another approach could be a web or mobile app that performs such pre-flight checks before allowing the user to place a bet. 

## Interface
A user of this SC does not want to send nativ Ethereum transactions but rather have an abstraction like a web or mobile app. This interface must provide a way to to interact with the SC in an usable way. A UI could list all upcoming games in a table and let the user select a game and place a bet. 

## Exchanging the Bet-ID
The bet ID is the confirmation ID a user receives when initiating a bet. The user needs to manually store this ID and pass it on to whoever wants to participate in this bet. How can we achieve this? One solution could be to have the ID sent out via email. Another option is to store it in a central DB hosted on a server.

## Match finish event
There are two ways to realize the match end trigger. One is using Oraclize and trigger a future event to query for the result when the match is over. The second approach is to send the result from outside the Blockchain to the SC.


# Errors and Issues
_Error: Exceeds block gas limit:_
Solution:
* check current block gas limit by running - eth.getBlock("latest"). If you supply more gas than this value, you will face 'Exceeds block gas limit' error
* Then check gas value in sendTransaction(). It should be atleast 21000. If it isn't, you will get 'intrinsic value too low'


    > truffle console
    truffle(development)> web3.eth.getBlock('latest').gasLimit
This will show current block gas limit. If sending ether do not send more than this

# Gas price and limit
## Set gas price via migration
var Voting = artifacts.require("./Voting.sol");
module.exports = function(deployer) {
  deployer.deploy(Voting, ['Rama', 'Nick', 'Jose'], {gas: 5000000});
};

truffle.js require('babel-register')

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      // gas:400000000000,
      gas:1000258612000000000,
      from: "0xf212bb926f7a831ff745e4236fc704a9947de77c"
    }
  }
}

deployer.deploy is just the maximum gas value you are allowing for the transaction. This is also above the block limit of 4m, so fix that too.

Since you are doing this on a test network, you can modify your genesis block to have a gas limit of 0x2fefd8 which is the same as the main network (3,141,592 gas). You will also need to reset that test chain since you'll be changing the genesis block.

## Set gas price via contract
Set custom gas price to 4 Gwei
    function ExampleContract() payable {
        oraclize_setCustomGasPrice(4000000000);
        LogConstructorInitiated("Constructor was initiated. Call 'updatePrice()' to send the Oraclize Query.");
    }

"You can modify your genesis block to have a gas limit of 0x2fefd8 which is the same as the main network (3,141,592 gas). You will also need to reset that test chain since you'll be changing the genesis block."


## Show current block gas limit
> truffle console
truffle(development)> web3.eth.getBlock('latest').gasLimit
This will show current block gas limit. If sending ether do not send more than this


# Soccer API
curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/competitions/BL1/teams | grep shortName

curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/competitions/BL1/matches

curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/competitions/BL1/standings

curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/competitions/BL1/matches?matchday=6 | grep Frankfurt

curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/matches/235731

## Pretty print
curl -H "X-Auth-Token: 2dd267c2daed49eb9f1ba4b40ef89347" -X GET http://api.football-data.org/v2/matches/235731 | jq '.'

# Random.org API using JSON RPC
curl https://api.o-X POST --data '{"jsonrpc":"2.0","method":"generateIntegers","params":{"apiKey": "b6f95201-7f37-4ecc-a897-33db429fa65f","n":1,"min":0,"max":10,"replacement":true,"base":10},"id":1}' -H 'content-type:application/json;' https://api.random.org/json-rpc/1/invoke