// /*
//     Diesel Price Peg

//     This contract keeps in storage a reference
//     to the Diesel Price in USD
// */
pragma solidity ^0.4.24;

// import "./imported/usingOraclize.sol";

// contract DieselPrice is usingOraclize {

//     uint public dieselPriceUSD;
//     bytes32 queryid;

//     struct game {
// 		string home; 
// 		string away;
// 		uint goalsHomeTeam;
// 		uint goalsAwayTeam;
// 		uint timeStamp;
//     }

// 	mapping (address => game) gameBets;

//     event NewOraclizeQuery(string description);
//     event NewDieselPrice(string price);
//     event callbackid(bytes32 id);
//     event setBetCallback(string team);

//     constructor () public {
//         OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
//     }

//     event newOraclizeQuery(string description);
//     event emitResult(string result);


//     function __callback(bytes32 myid, string result) {
//         if (msg.sender != oraclize_cbAddress()) throw;

//         emitResult(result);
//     }

//     // function request(string _query, string _method, string _url, string _kwargs) payable {
//     //     if (oraclize_getPrice("computation") > this.balance) {
//     //         newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
//     //     } else {
//     //         newOraclizeQuery("Oraclize query was sent, standing by for the answer...");

//     //     oraclize_query("nested", "[URL] ['json(https://api.random.org/json-rpc/1/invoke).result.random.data.0', '\\n{\"headers\":{\"content-type\":\"json\", 'X-Auth-Token' : 2dd267c2daed49eb9f1ba4b40ef89347}\"}\"}');    
//     //         // oraclize_query("computation",
//     //         //     [_query,
//     //         //     _method,
//     //         //     _url,
//     //         //     _kwargs]
//     //         // );
//     //     }
//     // }

//     function requestCustomHeaders() payable {
//         oraclize_query("nested", "[URL] ['json(http://api.football-data.org/v2/matches/235731).match.id', '\\n{\"headers\":{\"content-type\":\"json\", \"X-Auth-Token\" : 2dd267c2daed49eb9f1ba4b40ef89347}}']");   


//         // oraclize_query("nested", 
//         // "[URL] ['json(https://api.random.org/json-rpc/1/invoke).result.random.data.0', '\\n{\"jsonrpc\":\"2.0\",\"method\":\"generateIntegers\",\"params\":{\"apiKey\":\"${[decrypt] BC0g0vRNXBsncsjq5Yv4ZMiv7FvgnjfCKCyGhumP76fxZv+AiX6oZMVaFSy9ODnFBJ/kqZpLgcsYPfvZlIalwZ97LpJxMJ4AWib/mnfUunYLF6jZ0D0AKpQ2+4g1PdlJ4yh0W0qDHUdALsn2mEjAFqJRVmNw}\",\"n\":1,\"min\":1,\"max\":5,\"replacement\":true${[identity] \"}\"},\"id\":1${[identity] \"}\"}']");     

//         // request("json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).headers",
//         //         "GET",
//         //         "http://httpbin.org/headers",
//         //         "{'headers': {'content-type': 'json'}}"
//         //         );
//     }

//     // // sends a custom content-type in header and returns the header used as result
//     // // wrap first arguement of computation ds with helper needed, such as json in this case
//     // function requestCustomHeaders() payable {
//     //     request("json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).head2head",
//     //             "GET",
//     //             "http://api.football-data.org/v2/matches/235731",
//     //             "{'headers': {'content-type': 'json', 'X-Auth-Token' : 2dd267c2daed49eb9f1ba4b40ef89347}}"
//     //             );
//     // }

// 	// function setBet(uint gameId, uint goalsHomeTeam, uint goalsAwayTeam) public {
		

//     //     queryid = oraclize_query("URL", "xml(https://www.fueleconomy.gov/ws/rest/fuelprices).match.status");

        

//     //     gameBets[queryid] = game(gameId, goalsHomeTeam, goalsAwayTeam, msg.sender);
// 	// }

//     // // Fixed name; all callbacks from oraclize will end up here
//     // function __callback(bytes32 myid, string result) public {
//     //     callbackid(myid);
//     //     if (msg.sender != oraclize_cbAddress()) revert();
        
//     //             // Check ID formatting: < 2^31
//     //     // If ID not valid error code 400
//     //     // Check if status is either FINISHED or SCHEDULED
//     //     NewOraclizeQuery("Bet Sucessfully placed", gameId);
//     //     gameBets[queryid].isValid = True;
//     //     NewDieselPrice(result);

//     //     dieselPriceUSD = parseInt(result, 2);
//     // }

//     // // It's payable because we need funds to send to the contract to process the query
//     // function update() public payable {

//     //     queryid = oraclize_query("URL", "xml(https://www.fueleconomy.gov/ws/rest/fuelprices).fuelPrices.diesel");
//     //     NewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
//     // }
// } 
// // contract DieselPrice is usingOraclize {

// //    string public ETHUSD;
// //    event LogConstructorInitiated(string nextStep);
// //    event LogPriceUpdated(string price);
// //    event LogNewOraclizeQuery(string description);

// //    function ExampleContract() payable {
// //        LogConstructorInitiated("Constructor was initiated. Call 'updatePrice()' to send the Oraclize Query.");
// //    }

// //    function __callback(bytes32 myid, string result) {
// //        if (msg.sender != oraclize_cbAddress()) revert();
// //        ETHUSD = result;
// //        LogPriceUpdated(result);
// //    }

// //    function updatePrice() payable {
// //        if (oraclize_getPrice("URL") > this.balance) {
// //            LogNewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //        } else {
// //            LogNewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
// //            oraclize_query("URL", "json(https://api.gdax.com/products/ETH-USD/ticker).price");
// //        }
// //    }
// // }

// // contract DieselPrice is usingOraclize {

// //     uint public dieselPriceUSD;
// //     uint public randomNumber;
// //     bytes32 public queryIdPrice;
// //     bytes32 public queryIdRandom;
// //     event NewOraclizeQuery(string description);
// //     event CTORExecuted(string description);
// //     event NewDieselPrice(string price);
// //     event CallBackExec(bytes32 id);

// //     constructor () public {
// //         // Only needed when developing on private chain
// //         // Is provide by ethereum-bridge service
// //         OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
// //         update(); // first check at contract creation
// //         // CTORExecuted("CTOR done");
// //     }

// //     function __callback(bytes32 myid, string result) public {
// //         NewOraclizeQuery("hello1");
// //         // if (msg.sender != oraclize_cbAddress()) revert();
// //         NewOraclizeQuery("hello2");

// //         // CallBackExec(myid);

// //         // if (myid == queryIdRandom) {
// //         //     randomNumber = parseInt(result, 2);
// //         // }
        
// //         // if (myid == queryIdPrice) {
// //             NewDieselPrice(result);
// //             dieselPriceUSD = parseInt(result, 2);
// //         // }
// //     }

// //     function update() public payable {
// //         // if (oraclize_getPrice("URL") > this.balance) {
// //             // NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //         // } else {
// //             NewOraclizeQuery("Oraclize query Diesel Price");
// //             oraclize_query("URL", "xml(https://www.fueleconomy.gov/ws/rest/fuelprices).fuelPrices.diesel");
// //         // }
        
// //     }
// //     function random() public payable {
// //         CTORExecuted("CTOR done");
// //         if (oraclize_getPrice("URL") > this.balance) {
// //             NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //         } else {
// //             NewOraclizeQuery("Oraclize query Random Number");
// //             queryIdRandom = oraclize_query("WolframAlpha", "random number between 0 and 100");
// //         }
// //     }
// // }