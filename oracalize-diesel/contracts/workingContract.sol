// /*
//     Diesel Price Peg

//     This contract keeps in storage a reference
//     to the Diesel Price in USD
// */
pragma solidity ^0.4.24;

// import "./imported/usingOraclize.sol";

// contract DieselPrice is usingOraclize {

//     uint public dieselPriceUSD;
//     bytes32 queryid;

//     event NewOraclizeQuery(string description);
//     event NewDieselPrice(string price);
//     event callbackid(bytes32 id);

//     constructor () public {
//         OAR = OraclizeAddrResolverI(0x4E4745A686dd3FD28a691392E2a7726616adC89D);
//         // First check at contract creation. This call is free and does not
//         // need any funds to be available
//         update(); 
//     }

//     // Fixed name; all callbacks from oraclize will end up here
//     function __callback(bytes32 myid, string result) public {
//         callbackid(myid);
//         if (msg.sender != oraclize_cbAddress()) revert();
//         NewDieselPrice(result);
//         dieselPriceUSD = parseInt(result, 2);
//     }

//     // It's payable because we need funds to send to the contract to process the query
//     function update() public payable {

//         queryid = oraclize_query("URL", "xml(https://www.fueleconomy.gov/ws/rest/fuelprices).fuelPrices.diesel");
//         NewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
//     }
// } 
// // contract DieselPrice is usingOraclize {

// //    string public ETHUSD;
// //    event LogConstructorInitiated(string nextStep);
// //    event LogPriceUpdated(string price);
// //    event LogNewOraclizeQuery(string description);

// //    function ExampleContract() payable {
// //        LogConstructorInitiated("Constructor was initiated. Call 'updatePrice()' to send the Oraclize Query.");
// //    }

// //    function __callback(bytes32 myid, string result) {
// //        if (msg.sender != oraclize_cbAddress()) revert();
// //        ETHUSD = result;
// //        LogPriceUpdated(result);
// //    }

// //    function updatePrice() payable {
// //        if (oraclize_getPrice("URL") > this.balance) {
// //            LogNewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //        } else {
// //            LogNewOraclizeQuery("Oraclize query was sent, standing by for the answer..");
// //            oraclize_query("URL", "json(https://api.gdax.com/products/ETH-USD/ticker).price");
// //        }
// //    }
// // }

// // contract DieselPrice is usingOraclize {

// //     uint public dieselPriceUSD;
// //     uint public randomNumber;
// //     bytes32 public queryIdPrice;
// //     bytes32 public queryIdRandom;
// //     event NewOraclizeQuery(string description);
// //     event CTORExecuted(string description);
// //     event NewDieselPrice(string price);
// //     event CallBackExec(bytes32 id);

// //     constructor () public {
// //         // Only needed when developing on private chain
// //         // Is provide by ethereum-bridge service
// //         OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
// //         update(); // first check at contract creation
// //         // CTORExecuted("CTOR done");
// //     }

// //     function __callback(bytes32 myid, string result) public {
// //         NewOraclizeQuery("hello1");
// //         // if (msg.sender != oraclize_cbAddress()) revert();
// //         NewOraclizeQuery("hello2");

// //         // CallBackExec(myid);

// //         // if (myid == queryIdRandom) {
// //         //     randomNumber = parseInt(result, 2);
// //         // }
        
// //         // if (myid == queryIdPrice) {
// //             NewDieselPrice(result);
// //             dieselPriceUSD = parseInt(result, 2);
// //         // }
// //     }

// //     function update() public payable {
// //         // if (oraclize_getPrice("URL") > this.balance) {
// //             // NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //         // } else {
// //             NewOraclizeQuery("Oraclize query Diesel Price");
// //             oraclize_query("URL", "xml(https://www.fueleconomy.gov/ws/rest/fuelprices).fuelPrices.diesel");
// //         // }
        
// //     }
// //     function random() public payable {
// //         CTORExecuted("CTOR done");
// //         if (oraclize_getPrice("URL") > this.balance) {
// //             NewOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
// //         } else {
// //             NewOraclizeQuery("Oraclize query Random Number");
// //             queryIdRandom = oraclize_query("WolframAlpha", "random number between 0 and 100");
// //         }
// //     }
// // }