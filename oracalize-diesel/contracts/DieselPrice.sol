/*
    Diesel Price Peg

    This contract keeps in storage a reference
    to the Diesel Price in USD
*/
pragma solidity ^0.4.24;

import "./imported/usingOraclize.sol";

contract DieselPrice is usingOraclize {

    struct game {
        string matchId;
		string goalsHomeTeam;
		string goalsAwayTeam;
		bool valid;
        address sender;
        bool exists;
        bytes32 ref;
    }

    mapping (bytes32 => game) public gameBets;
    bytes32 public result_query  = "";

    event requestBet(string matchId);
    event debug(string msg);
    event callbackInvocation(bytes32 id, string result);
    event joinBetEvent(string msg);

    // First check at contract creation. This call is free and does not
    // need any funds to be available
    constructor () public payable {
        OAR = OraclizeAddrResolverI(0x0dF7186434dB085110257a1BE073d31363d852b8);
        // oraclize_setCustomGasPrice(4000000000);
        // oraclize_setProof(proofType_NONE );

        // Call this to init the Oraclize state variables
        oraclize_setNetwork(networkID_auto);
        // debug("CTOR QUERY");
        // request_result_delay(5, "235767");
    }

    // Fixed name; all callbacks from oraclize will end up here
    function __callback(bytes32 myid, string result) public {
        if (msg.sender != oraclize_cbAddress()) revert();

        emit callbackInvocation(myid, result);

        if (result_query != "") {
            debug("Received result");
            result_query = "";
            return;
        }

        require(gameBets[myid].exists, "Person does not exist.");

        if (strCompare(result, "SCHEDULED") == 0) {
            gameBets[myid].valid = true;
            debug("Bet successfull");
        } else if (strCompare(result, "FINISHED") == 0) {
            gameBets[myid].valid = false;
            debug("Too late to bet");
        }

        if (gameBets[myid].ref != "") {
            debug("Found ref bet");
            result_query = request_result_delay(5, "235767");
        }
    }

    function request_result_delay(uint delay, string matchId) public payable  returns(bytes32) {
        if (oraclize_getPrice("URL") > this.balance) {
           debug("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            // TODO: encrypt via Oraclize pub key
            string memory header = "{'headers': {'X-Auth-Token': '2dd267c2daed49eb9f1ba4b40ef89347'}}";
            string memory url = strConcat("http://api.football-data.org/v2/matches/", matchId );
            bytes32 queryId = request("json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE)[match][score][winner]",
                    "GET",
                    url,
                    header);
            return queryId;
       }
    }

    function request_delay(uint delay, string _query, string _method, string _url, string _header) public payable  returns(bytes32) {
        bytes32 queryId = oraclize_query(delay, "computation",
            [_query,
            _method,
            _url,
            _header]
        );
        return queryId;
    }

    function requestMatchState(string matchId) public payable returns(bytes32) {
        if (oraclize_getPrice("URL") > this.balance) {
           debug("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
       } else {
            // TODO: encrypt via Oraclize pub key
            string memory header = "{'headers': {'X-Auth-Token': '2dd267c2daed49eb9f1ba4b40ef89347'}}";
            string memory url = strConcat("http://api.football-data.org/v2/matches/", matchId);
            bytes32 queryId = request("json(QmdKK319Veha83h6AYgQqhx9YRsJ9MJE7y33oCXyZ4MqHE).match.status",
                    "GET",
                    url,
                    header);
            requestBet(matchId);
            return queryId;
       }
    }

    function request(string _query, string _method, string _url, string _header) public payable  returns(bytes32) {
        bytes32 queryId = oraclize_query("computation",
            [_query,
            _method,
            _url,
            _header]
        );
        return queryId;
    }

    // Initiate a new bet
    function setBet(string matchId, string goalsHomeTeam, string goalsAwayTeam) public payable {
        bytes32 queryId = requestMatchState(matchId);
        gameBets[queryId] = game(matchId, goalsHomeTeam, goalsAwayTeam, false, msg.sender, true, "");
    }
    
    // Participate in an existing bet
    function joinBet(bytes32 betId, string matchId, string goalsHomeTeam, string goalsAwayTeam) public payable {
        if (gameBets[betId].valid == true && (strCompare(gameBets[betId].matchId, matchId) == 0)) {
            bytes32 queryId = requestMatchState(matchId);
            gameBets[queryId] = game(matchId, goalsHomeTeam, goalsAwayTeam, false, msg.sender, true, betId);
            joinBetEvent("Bet to join found");
        } else {
            joinBetEvent("Bet to join not found");
        }
    }
} 
