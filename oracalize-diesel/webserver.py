from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)


# Todo
# shows a single todo item and lets you delete a todo item
class Endpoint(Resource):
    def get(self):
        return "<fuelPrices>   \
            <cng>2.18</cng>\
            <diesel>3.27</diesel>\
            <e85>2.21</e85>\
            <electric>0.13</electric>\
            <lpg>2.83</lpg>\
            <midgrade>3.2</midgrade>\
            <premium>3.44</premium>\
            <regular>2.84</regular>\
            </fuelPrices>"

    def delete(self, todo_id):
        return '', 204

    def put(self, todo_id):
        return "", 201


from flask_restful import Resource, fields, marshal_with

api.add_resource(Endpoint, '/')


if __name__ == '__main__':
    app.run(debug=True, port=443)