const artifacts = require('../build/contracts/DieselPrice.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

module.exports = function(callback) {
    var meta;
    var contract;
    var creator = web3.eth.accounts[0]
    var purchaser = web3.eth.accounts[1]
    // 235731 FINISHED
    // 235953 SCHEDULED
    var matchid = "235953";

    MyContract.deployed().then(function(instance) {
        meta = instance;
        contract = instance.address;
        console.log("Contract address:" + contract)
        return meta.setBet.sendTransaction("235953","2","1",{from: creator, value: web3.toWei(1,"ether"), gas: 3000000});
    }).then(function(result) {
        console.log(result)
    }).catch(function(e) {
        console.log(e)
    })
}
