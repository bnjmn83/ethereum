const artifacts = require('../build/contracts/DieselPrice.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

module.exports = function(callback) {
    var meta;
    var contract;
    var creator = web3.eth.accounts[0]
    var purchaser = web3.eth.accounts[1]
    // 235731 FINISHED
    // 235953 SCHEDULED
    var matchId = "235953";
    var betId = process.argv[4];

    console.log("Given Bet ID " + betId);

    MyContract.deployed().then(function(instance) {
        meta = instance;
        contract = instance.address;
        console.log("Contract address: " + contract)
        return meta.gameBets.call(betId);
    }).then(function(result) {
        console.log("GameBet mapping for given Bet ID " + result)
        return meta.joinBet.sendTransaction(betId, matchId, "3", "1",{from: creator, value: web3.toWei(1,"ether"), gas: 3000000})        
    }).then(function(result) {
        console.log(result)
    }).catch(function(e) {
        console.log(e)
    })
}
