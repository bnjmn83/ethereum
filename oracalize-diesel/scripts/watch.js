const artifacts = require('../build/contracts/DieselPrice.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

// function myCallback(err, result) {
//     console.log(result);
// }

// Here we just perform the request to the contract to trigger oracle interaction
module.exports = function(callback) {
    var meta;

    MyContract.deployed().then(function(instance) {
        meta = instance;

        var events = meta.allEvents();

        // watch for changes
        events.watch(function(error, result){
        if (!error) {
            if(['requestBet'].includes(result.event)) {
                console.log(result);
            }
            if(['callbackInvocation'].includes(result.event)) {
                console.log(result);
            }
            if(['validatedBet'].includes(result.event)) {
                console.log(result);
            }            
            if(['debug'].includes(result.event)) {
                console.log(result);
            }            
            if(['joinBetEvent'].includes(result.event)) {
                console.log(result);
            }            
        }
        })

        // TODO:
        // event.stopWatching()

    // var event1 = MyContract.CallBackExec();
    // var event2 = MyContract.NewOraclizeQuery();

    // event1.watch(myCallback);
    // event2.watch(myCallback);



        // meta.CallBackExec()
        // .watch((err, event) => {
        //     if (!err)
        //         console.log(event)
        // })

        // meta.NewOraclizeQuery()
        // .watch((err, event) => {
        //     if (!err)
        //         console.log(event)
        // })

        // meta.CTORExecuted()
        // .watch((err, event) => {
        //     if (!err)
        //         console.log(event)
        // })


    }).catch(function(e) {
        console.log(e)
    })
}


// // Get accounts from web3
// web3.eth.getAccounts((err, accounts) => {
//   oracleContract.deployed()
//   .then((oracleInstance) => {
//     // Watch event and respond to event
//     // With a callback function  
//     oracleInstance.CallbackGetBTCCap()
//     .watch((err, event) => {
//       // Fetch data
//       // and update it into the contract
//       fetch.fetchUrl('https://api.coinmarketcap.com/v1/global/', (err, m, b) => {
//         const cmcJson = JSON.parse(b.toString())
//         const btcMarketCap = parseInt(cmcJson.total_market_cap_usd)

//         // Send data back contract on-chain
//         oracleInstance.setBTCCap(btcMarketCap, {from: accounts[0]})
//       })
//     })
//   })
//   .catch((err) => {
//     console.log(err)
//   })
// })