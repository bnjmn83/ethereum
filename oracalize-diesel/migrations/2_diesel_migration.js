
const contracts = [
  artifacts.require("./strings.sol"),
  artifacts.require("./usingOraclize.sol"),
]
const gasOracle = artifacts.require("./DieselPrice.sol")

module.exports = deployer => {
  const gasPrice  = 2e10
  const amountETH = 1e17
  contracts.map(contract => deployer.deploy(contract))
  deployer.deploy(gasOracle)
  // This passes the argument gasPrice to the ctor of the contract and
  // also sends some ETH 
  // deployer.deploy(gasOracle, gasPrice, {value: amountETH})
}

