# truffle migrate --compile-all --reset
# truffle build --all
# truffle compile --all
# truffle migrate --reset
# truffle debug TXHash
# truffle console
## Send ether to contract
web3.eth.sendTransaction({from: web3.eth.accounts[3], to: "", value: web3.toWei(1, "ether")});

## Read from contract at its address
benstoken.at("").balanceOf(web3.eth.accounts[3])
benstoken.at("").totalSupply()
benstoken.at("").balanceOf(web3.eth.accounts[0]).then(function(res){return res.toString();})

# Using promise objects; those are return by then(). function(res) gets invoked if promised gets fullfilled.
benstoken.at("").then(function(res) { return res.totalSupply(); }).then(function(result){console.log("got balance", result.toString(10));});

# Change sender of transaction in truffle
benstoken.at("0x847b2745c73355fd041a3ca269981e51167a64b2").acceptOwnership({from: web3.eth.accounts[1]})

# Ownership
# Transfer From
## 1. Approve sender 0x71 to send tokens of 1 Ether in total
benstoken.at("").then(function(res) { return res.approve("", web3.toWei(1, "ether"));}).then(function(result){console.log("cool");});

## 2. Transfer from 0x71 to 0x26 of max 1 ether; this is what we approved before
benstoken.at("").then(function(res) { return res.transferFrom("", "", web3.toWei(1, "ether"));}).then(function(result){console.log("cool");});

