pragma solidity ^0.4.17;

contract CMCOracle {
    // Contract owner
    address public owner;

    // BTC Marketcap Storage
    uint public btcMarketCap;

    // Callback function; We can listen to this event from within the oracle
    event CallbackGetBTCCap();

    constructor() public {
        owner = msg.sender;
    }

    // User can trigger this function with any transaction/call
    function updateBTCCap() public {
        // Calls the callback function
        emit CallbackGetBTCCap();
    }

    function setBTCCap(uint cap) public {
        // If it isn't sent by a trusted oracle
        // a.k.a ourselves, ignore it
        require(msg.sender == owner);
        btcMarketCap = cap;
    }

    function getBTCCap() public view returns (uint) {
        return btcMarketCap;
    }
}