const artifacts = require('../build/contracts/CMCOracle.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

module.exports = function(callback) {
    var meta;

    MyContract.deployed().then(function(instance) {
        meta = instance;
        return meta.getBTCCap.call();
    }).then(function(result) {
        console.log("btc cap" + result)
    }).catch(function(e) {
        console.log(e)
    })
}