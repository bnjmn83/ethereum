const artifacts = require('./build/contracts/CMCOracle.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

// Here we just perform the request to the contract to trigger oracle interaction
module.exports = function(callback) {

    var meta;

    MyContract.deployed().then(function(instance) {
        meta = instance;

        // Our promises
        const oraclePromises = [
            meta.getBTCCap.call(),  // Get currently stored BTC Cap
            meta.updateBTCCap.call({from: web3.eth.accounts[0]})  // Request oracle to update the information
        ]
        
        // Map over all promises
        Promise.all(oraclePromises)
        .then((result) => {
            console.log('BTC Market Cap: ' + result[0])
            console.log('Requesting Oracle to update CMC Information...')
        })
        .catch((err) => {
            console.log(err)
        })

    }).catch(function(e) {
        console.log(e)
    })
}