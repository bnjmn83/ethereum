const artifacts = require('./build/contracts/CMCOracle.json')
const contract = require('truffle-contract')
const MyContract = contract(artifacts);
MyContract.setProvider(web3.currentProvider);

// Here we just perform the request to the contract to trigger oracle interaction
module.exports = function(callback) {
    var meta;

    MyContract.deployed().then(function(instance) {
        meta = instance;

        // Watch event from contract and respond to event
        // with a callback function  
        meta.CallbackGetBTCCap()
        .watch((err, event) => {
            // Fetch data and update it into the contract
            fetch.fetchUrl('https://api.coinmarketcap.com/v1/global/', (err, m, b) => {
              	const cmcJson = JSON.parse(b.toString())
              	const btcMarketCap = parseInt(cmcJson.total_market_cap_usd)
              	// Send data back contract on-chain; needs to be the same account which deployed it to the ledger
            	meta.setBTCCap(btcMarketCap, {from: accounts[0]})
          	})
        })
        .catch((err) => {
            console.log(err)
        })
    }).catch(function(e) {
        console.log(e)
    })
}


// // Get accounts from web3
// web3.eth.getAccounts((err, accounts) => {
//   oracleContract.deployed()
//   .then((oracleInstance) => {
//     // Watch event and respond to event
//     // With a callback function  
//     oracleInstance.CallbackGetBTCCap()
//     .watch((err, event) => {
//       // Fetch data
//       // and update it into the contract
//       fetch.fetchUrl('https://api.coinmarketcap.com/v1/global/', (err, m, b) => {
//         const cmcJson = JSON.parse(b.toString())
//         const btcMarketCap = parseInt(cmcJson.total_market_cap_usd)

//         // Send data back contract on-chain
//         oracleInstance.setBTCCap(btcMarketCap, {from: accounts[0]})
//       })
//     })
//   })
//   .catch((err) => {
//     console.log(err)
//   })
// })